import Vue from 'vue';

var script = Vue.extend({
  name: 'NavBar',
  props: {
    background: {
      required: false,
      default: 'white'
    },
    elements: {
      required: true
    },
    fixed: {
      required: false,
      default: false
    },
    textColor: {
      required: false,
      default: 'black'
    }
  },

  data() {
    return {
      showMenu: false
    };
  },

  computed: {
    setBackground() {
      return {
        background: `${this.background}`
      };
    },

    setFixed() {
      return this.fixed ? 'fixed' : '';
    },

    setTextColor() {
      return {
        color: `${this.textColor}`
      };
    }

  },

  created() {
    window.addEventListener('resize', () => {
      if (window.innerWidth < 768) this.showMenu = false;
    });
  }

});

function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier /* server only */, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
    if (typeof shadowMode !== 'boolean') {
        createInjectorSSR = createInjector;
        createInjector = shadowMode;
        shadowMode = false;
    }
    // Vue.extend constructor export interop.
    const options = typeof script === 'function' ? script.options : script;
    // render functions
    if (template && template.render) {
        options.render = template.render;
        options.staticRenderFns = template.staticRenderFns;
        options._compiled = true;
        // functional template
        if (isFunctionalTemplate) {
            options.functional = true;
        }
    }
    // scopedId
    if (scopeId) {
        options._scopeId = scopeId;
    }
    let hook;
    if (moduleIdentifier) {
        // server build
        hook = function (context) {
            // 2.3 injection
            context =
                context || // cached call
                    (this.$vnode && this.$vnode.ssrContext) || // stateful
                    (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext); // functional
            // 2.2 with runInNewContext: true
            if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
                context = __VUE_SSR_CONTEXT__;
            }
            // inject component styles
            if (style) {
                style.call(this, createInjectorSSR(context));
            }
            // register component module identifier for async chunk inference
            if (context && context._registeredComponents) {
                context._registeredComponents.add(moduleIdentifier);
            }
        };
        // used by ssr in case component is cached and beforeCreate
        // never gets called
        options._ssrRegister = hook;
    }
    else if (style) {
        hook = shadowMode
            ? function (context) {
                style.call(this, createInjectorShadow(context, this.$root.$options.shadowRoot));
            }
            : function (context) {
                style.call(this, createInjector(context));
            };
    }
    if (hook) {
        if (options.functional) {
            // register for functional component in vue file
            const originalRender = options.render;
            options.render = function renderWithStyleInjection(h, context) {
                hook.call(context);
                return originalRender(h, context);
            };
        }
        else {
            // inject component registration as beforeCreate hook
            const existing = options.beforeCreate;
            options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
    }
    return script;
}

const isOldIE = typeof navigator !== 'undefined' &&
    /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());
function createInjector(context) {
    return (id, style) => addStyle(id, style);
}
let HEAD;
const styles = {};
function addStyle(id, css) {
    const group = isOldIE ? css.media || 'default' : id;
    const style = styles[group] || (styles[group] = { ids: new Set(), styles: [] });
    if (!style.ids.has(id)) {
        style.ids.add(id);
        let code = css.source;
        if (css.map) {
            // https://developer.chrome.com/devtools/docs/javascript-debugging
            // this makes source maps inside style tags work properly in Chrome
            code += '\n/*# sourceURL=' + css.map.sources[0] + ' */';
            // http://stackoverflow.com/a/26603875
            code +=
                '\n/*# sourceMappingURL=data:application/json;base64,' +
                    btoa(unescape(encodeURIComponent(JSON.stringify(css.map)))) +
                    ' */';
        }
        if (!style.element) {
            style.element = document.createElement('style');
            style.element.type = 'text/css';
            if (css.media)
                style.element.setAttribute('media', css.media);
            if (HEAD === undefined) {
                HEAD = document.head || document.getElementsByTagName('head')[0];
            }
            HEAD.appendChild(style.element);
        }
        if ('styleSheet' in style.element) {
            style.styles.push(code);
            style.element.styleSheet.cssText = style.styles
                .filter(Boolean)
                .join('\n');
        }
        else {
            const index = style.ids.size - 1;
            const textNode = document.createTextNode(code);
            const nodes = style.element.childNodes;
            if (nodes[index])
                style.element.removeChild(nodes[index]);
            if (nodes.length)
                style.element.insertBefore(textNode, nodes[index]);
            else
                style.element.appendChild(textNode);
        }
    }
}

/* script */
const __vue_script__ = script;
/* template */

var __vue_render__ = function () {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('nav', {
    class: _vm.setFixed,
    style: _vm.setBackground
  }, [_c('button', {
    staticClass: "mobile-btn",
    attrs: {
      "role": "button"
    },
    on: {
      "click": function ($event) {
        _vm.showMenu = !_vm.showMenu;
      }
    }
  }, [_c('div', {
    staticClass: "mobile-btn_stripe",
    style: {
      background: _vm.textColor || 'inherit'
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "mobile-btn_stripe",
    style: {
      background: _vm.textColor || 'inherit'
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "mobile-btn_stripe",
    style: {
      background: _vm.textColor || 'inherit'
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "nav-container"
  }, _vm._l(_vm.elements, function (element) {
    return _c('a', {
      key: element.title,
      style: _vm.setTextColor,
      attrs: {
        "href": element.href || '#'
      }
    }, [_vm._v("\n      " + _vm._s(element.title) + "\n    ")]);
  }), 0), _vm._v(" "), _vm.showMenu ? _c('div', {
    staticClass: "nav-container_mobile",
    style: _vm.setBackground
  }, _vm._l(_vm.elements, function (element) {
    return _c('a', {
      key: element.title,
      style: _vm.setTextColor,
      attrs: {
        "href": element.href || '#'
      }
    }, [_vm._v("\n      " + _vm._s(element.title) + "\n    ")]);
  }), 0) : _vm._e()]);
};

var __vue_staticRenderFns__ = [];
/* style */

const __vue_inject_styles__ = function (inject) {
  if (!inject) return;
  inject("data-v-30c883de_0", {
    source: "nav[data-v-30c883de]{display:block;padding:1rem 0;-webkit-box-shadow:0 14px 15px -10px rgba(0,0,0,.75);box-shadow:0 14px 15px -10px rgba(0,0,0,.75);height:2rem;width:100%}nav a[data-v-30c883de]{text-decoration:none;color:inherit;margin:0 1rem}nav a[data-v-30c883de]:hover{font-weight:700}.mobile-btn[data-v-30c883de]{width:1.5rem;margin-left:1rem;border:none;padding:0;background:0 0;cursor:pointer}@media (min-width:768px){.mobile-btn[data-v-30c883de]{display:none}}.mobile-btn_stripe[data-v-30c883de]{height:.2rem;margin:.3rem 0}.nav-container[data-v-30c883de]{display:flex;justify-content:center;align-items:center;height:100%}@media (max-width:768px){.nav-container[data-v-30c883de]{display:none}}.nav-container_mobile[data-v-30c883de]{position:relative;display:flex;flex-direction:column;justify-content:center;align-items:center;width:100%;padding:1.5rem 0}@media (min-width:768px){.nav-container_mobile[data-v-30c883de]{display:none}}.nav-container_mobile a[data-v-30c883de]{margin:1rem 0}.fixed[data-v-30c883de]{position:fixed;top:0}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


const __vue_scope_id__ = "data-v-30c883de";
/* module identifier */

const __vue_module_identifier__ = undefined;
/* functional template */

const __vue_is_functional_template__ = false;
/* style inject SSR */

/* style inject shadow dom */

const __vue_component__ = /*#__PURE__*/normalizeComponent({
  render: __vue_render__,
  staticRenderFns: __vue_staticRenderFns__
}, __vue_inject_styles__, __vue_script__, __vue_scope_id__, __vue_is_functional_template__, __vue_module_identifier__, false, createInjector, undefined, undefined);

// Import vue component

// Default export is installable instance of component.
// IIFE injects install function into component, allowing component
// to be registered via Vue.use() as well as Vue.component(),
var entry_esm = /*#__PURE__*/(() => {
  // Assign InstallableComponent type
  const installable = __vue_component__; // Attach install function executed by Vue.use()

  installable.install = Vue => {
    Vue.component('VueTsNavbarSimple', installable);
  };

  return installable;
})(); // It's possible to expose named exports when writing components that can
// also be used as directives, etc. - eg. import { RollupDemoDirective } from 'rollup-demo';
// export const RollupDemoDirective = directive;

export default entry_esm;
