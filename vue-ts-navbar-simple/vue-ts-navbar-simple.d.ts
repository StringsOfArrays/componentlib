import Vue, { PluginFunction, VueConstructor } from 'vue';

declare const VueTsNavbarSimple: VueConstructor<Vue> & { install: PluginFunction<any>; };
export default VueTsNavbarSimple;
