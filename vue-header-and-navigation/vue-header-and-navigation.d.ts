import Vue, { PluginFunction, VueConstructor } from 'vue';


declare const VueHeaderAndNavigation: PluginFunction<any>;
export default VueHeaderAndNavigation;

export const VueHeaderAndNavigationSample: VueConstructor<Vue>;
