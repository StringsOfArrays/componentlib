'use strict';var Vue=require('vue');function _interopDefaultLegacy(e){return e&&typeof e==='object'&&'default'in e?e:{'default':e}}var Vue__default=/*#__PURE__*/_interopDefaultLegacy(Vue);function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();
}

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

function _iterableToArrayLimit(arr, i) {
  var _i = arr && (typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]);

  if (_i == null) return;
  var _arr = [];
  var _n = true;
  var _d = false;

  var _s, _e;

  try {
    for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

  return arr2;
}

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}var script$1 = Vue__default['default'].extend({
  props: {
    heading: String
  }
});function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier /* server only */, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
    if (typeof shadowMode !== 'boolean') {
        createInjectorSSR = createInjector;
        createInjector = shadowMode;
        shadowMode = false;
    }
    // Vue.extend constructor export interop.
    const options = typeof script === 'function' ? script.options : script;
    // render functions
    if (template && template.render) {
        options.render = template.render;
        options.staticRenderFns = template.staticRenderFns;
        options._compiled = true;
        // functional template
        if (isFunctionalTemplate) {
            options.functional = true;
        }
    }
    // scopedId
    if (scopeId) {
        options._scopeId = scopeId;
    }
    let hook;
    if (moduleIdentifier) {
        // server build
        hook = function (context) {
            // 2.3 injection
            context =
                context || // cached call
                    (this.$vnode && this.$vnode.ssrContext) || // stateful
                    (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext); // functional
            // 2.2 with runInNewContext: true
            if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
                context = __VUE_SSR_CONTEXT__;
            }
            // inject component styles
            if (style) {
                style.call(this, createInjectorSSR(context));
            }
            // register component module identifier for async chunk inference
            if (context && context._registeredComponents) {
                context._registeredComponents.add(moduleIdentifier);
            }
        };
        // used by ssr in case component is cached and beforeCreate
        // never gets called
        options._ssrRegister = hook;
    }
    else if (style) {
        hook = shadowMode
            ? function (context) {
                style.call(this, createInjectorShadow(context, this.$root.$options.shadowRoot));
            }
            : function (context) {
                style.call(this, createInjector(context));
            };
    }
    if (hook) {
        if (options.functional) {
            // register for functional component in vue file
            const originalRender = options.render;
            options.render = function renderWithStyleInjection(h, context) {
                hook.call(context);
                return originalRender(h, context);
            };
        }
        else {
            // inject component registration as beforeCreate hook
            const existing = options.beforeCreate;
            options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
    }
    return script;
}/* script */
var __vue_script__$1 = script$1;
/* template */

var __vue_render__$1 = function __vue_render__() {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('h1', [_vm._ssrNode(_vm._ssrEscape(_vm._s(_vm.heading)))]);
};

var __vue_staticRenderFns__$1 = [];
/* style */

var __vue_inject_styles__$1 = undefined;
/* scoped */

var __vue_scope_id__$1 = undefined;
/* module identifier */

var __vue_module_identifier__$1 = "data-v-2c213a14";
/* functional template */

var __vue_is_functional_template__$1 = false;
/* style inject */

/* style inject SSR */

/* style inject shadow dom */

var __vue_component__$1 = /*#__PURE__*/normalizeComponent({
  render: __vue_render__$1,
  staticRenderFns: __vue_staticRenderFns__$1
}, __vue_inject_styles__$1, __vue_script__$1, __vue_scope_id__$1, __vue_is_functional_template__$1, __vue_module_identifier__$1, false, undefined, undefined, undefined);var script = Vue__default['default'].extend({
  name: 'NavBar',
  props: {
    background: {
      required: false,
      default: 'white'
    },
    elements: {
      required: true
    },
    fixed: {
      required: false,
      default: false
    },
    textColor: {
      required: false,
      default: 'black'
    }
  },
  data: function data() {
    return {
      showMenu: false
    };
  },
  computed: {
    setBackground: function setBackground() {
      return {
        background: "".concat(this.background)
      };
    },
    setFixed: function setFixed() {
      return this.fixed ? 'fixed' : '';
    },
    setTextColor: function setTextColor() {
      return {
        color: "".concat(this.textColor)
      };
    }
  },
  created: function created() {
    var _this = this;

    window.addEventListener('resize', function () {
      if (window.innerWidth < 768) _this.showMenu = false;
    });
  }
});function createInjectorSSR(context) {
    if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__;
    }
    if (!context)
        return () => { };
    if (!('styles' in context)) {
        context._styles = context._styles || {};
        Object.defineProperty(context, 'styles', {
            enumerable: true,
            get: () => context._renderStyles(context._styles)
        });
        context._renderStyles = context._renderStyles || renderStyles;
    }
    return (id, style) => addStyle(id, style, context);
}
function addStyle(id, css, context) {
    const group = css.media || 'default' ;
    const style = context._styles[group] || (context._styles[group] = { ids: [], css: '' });
    if (!style.ids.includes(id)) {
        style.media = css.media;
        style.ids.push(id);
        let code = css.source;
        style.css += code + '\n';
    }
}
function renderStyles(styles) {
    let css = '';
    for (const key in styles) {
        const style = styles[key];
        css +=
            '<style data-vue-ssr-id="' +
                Array.from(style.ids).join(' ') +
                '"' +
                (style.media ? ' media="' + style.media + '"' : '') +
                '>' +
                style.css +
                '</style>';
    }
    return css;
}/* script */
var __vue_script__ = script;
/* template */

var __vue_render__ = function __vue_render__() {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('nav', {
    class: _vm.setFixed,
    style: _vm.setBackground
  }, [_vm._ssrNode("<button role=\"button\" class=\"mobile-btn\" data-v-722e3fc6><div class=\"mobile-btn_stripe\"" + _vm._ssrStyle(null, {
    background: _vm.textColor || 'inherit'
  }, null) + " data-v-722e3fc6></div> <div class=\"mobile-btn_stripe\"" + _vm._ssrStyle(null, {
    background: _vm.textColor || 'inherit'
  }, null) + " data-v-722e3fc6></div> <div class=\"mobile-btn_stripe\"" + _vm._ssrStyle(null, {
    background: _vm.textColor || 'inherit'
  }, null) + " data-v-722e3fc6></div></button> <div class=\"nav-container\" data-v-722e3fc6>" + _vm._ssrList(_vm.elements, function (element) {
    return "<a" + _vm._ssrAttr("href", element.href || '#') + _vm._ssrStyle(null, _vm.setTextColor, null) + " data-v-722e3fc6>" + _vm._ssrEscape("\n      " + _vm._s(element.title) + "\n    ") + "</a>";
  }) + "</div> " + (_vm.showMenu ? "<div class=\"nav-container_mobile\"" + _vm._ssrStyle(null, _vm.setBackground, null) + " data-v-722e3fc6>" + _vm._ssrList(_vm.elements, function (element) {
    return "<a" + _vm._ssrAttr("href", element.href || '#') + _vm._ssrStyle(null, _vm.setTextColor, null) + " data-v-722e3fc6>" + _vm._ssrEscape("\n      " + _vm._s(element.title) + "\n    ") + "</a>";
  }) + "</div>" : "<!---->"))]);
};

var __vue_staticRenderFns__ = [];
/* style */

var __vue_inject_styles__ = function __vue_inject_styles__(inject) {
  if (!inject) return;
  inject("data-v-722e3fc6_0", {
    source: "nav[data-v-722e3fc6]{display:block;padding:1rem 0;-webkit-box-shadow:0 14px 15px -10px rgba(0,0,0,.75);box-shadow:0 14px 15px -10px rgba(0,0,0,.75);height:2rem;width:100%}nav a[data-v-722e3fc6]{text-decoration:none;color:inherit;margin:0 1rem}nav a[data-v-722e3fc6]:hover{font-weight:700}.mobile-btn[data-v-722e3fc6]{width:1.5rem;margin-left:1rem;border:none;padding:0;background:0 0;cursor:pointer}@media (min-width:768px){.mobile-btn[data-v-722e3fc6]{display:none}}.mobile-btn_stripe[data-v-722e3fc6]{height:.2rem;margin:.3rem 0}.nav-container[data-v-722e3fc6]{display:flex;justify-content:center;align-items:center;height:100%}@media (max-width:768px){.nav-container[data-v-722e3fc6]{display:none}}.nav-container_mobile[data-v-722e3fc6]{position:relative;display:flex;flex-direction:column;justify-content:center;align-items:center;width:100%;padding:1.5rem 0}@media (min-width:768px){.nav-container_mobile[data-v-722e3fc6]{display:none}}.nav-container_mobile a[data-v-722e3fc6]{margin:1rem 0}.fixed[data-v-722e3fc6]{position:fixed;top:0}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


var __vue_scope_id__ = "data-v-722e3fc6";
/* module identifier */

var __vue_module_identifier__ = "data-v-722e3fc6";
/* functional template */

var __vue_is_functional_template__ = false;
/* style inject shadow dom */

var __vue_component__ = /*#__PURE__*/normalizeComponent({
  render: __vue_render__,
  staticRenderFns: __vue_staticRenderFns__
}, __vue_inject_styles__, __vue_script__, __vue_scope_id__, __vue_is_functional_template__, __vue_module_identifier__, false, undefined, createInjectorSSR, undefined);/* eslint-disable import/prefer-default-export */var components$1=/*#__PURE__*/Object.freeze({__proto__:null,Header: __vue_component__$1,NavBar: __vue_component__});var install = function installVueHeaderAndNavigation(Vue) {
  Object.entries(components$1).forEach(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        componentName = _ref2[0],
        component = _ref2[1];

    Vue.component(componentName, component);
  });
}; // Create module definition for Vue.use()
var components=/*#__PURE__*/Object.freeze({__proto__:null,'default': install,Header: __vue_component__$1,NavBar: __vue_component__});// only expose one global var, with component exports exposed as properties of
// that global var (eg. plugin.component)

Object.entries(components).forEach(function (_ref) {
  var _ref2 = _slicedToArray(_ref, 2),
      componentName = _ref2[0],
      component = _ref2[1];

  if (componentName !== 'default') {
    var key = componentName;
    var val = component;
    install[key] = val;
  }
});module.exports=install;