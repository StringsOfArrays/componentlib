import Vue, { PluginFunction, VueConstructor } from 'vue';

declare const PleaseLetMeVuetify: VueConstructor<Vue> & { install: PluginFunction<any>; };
export default PleaseLetMeVuetify;
